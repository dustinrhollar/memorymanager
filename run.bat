@echo off

SET NONE_BUILD=""


GOTO :MAIN
EXIT /B %ERRORLEVEL%

:Default
	pushd build\
		mm_example.exe > log.txt
	popd
	EXIT /B 0

:MAIN
	IF "%1" == %NONE_BUILD% (
    	CALL :Default
		EXIT /B %ERRORLEVEL%
	)