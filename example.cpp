#include <jackal_types.h>

// Memory Manager source
#include <mm/allocator.h>
#include <mm/linear_allocator.h>
#include <mm/stack_allocator.h>
#include <mm/free_list_allocator.h>
#include <mm/pool_allocator.h>
#include <mm/proxy_allocator.h>
#include <mm/mm.h>

#include <mm/linear_allocator.cpp>
#include <mm/stack_allocator.cpp>
#include <mm/free_list_allocator.cpp>
#include <mm/pool_allocator.cpp>
#include <mm/proxy_allocator.cpp>
#include <mm/mm.cpp>

typedef struct 
{
    int a;
    int b;
    r32 c;
} CoolStruct;

using namespace jengine;
using namespace jengine::mm;

int main(void)
{
    InitializeMemoryManager(_128MB, _32MB);
    
    // Allocate raw memory in permanant storage works the same as clib malloc
    CoolStruct *cool1 = (CoolStruct*)jalloc(sizeof(CoolStruct));
    // freeing that memory works the same as clib free
    jfree(cool1);
    
    // Can also allocate in temporary storage too.
    cool1 = (CoolStruct*)jalloc(sizeof(CoolStruct), MEMORY_REQUEST_TRANSIENT_STORAGE);
    // But make sure not to call free on it!
    // jengine::mm::jfree(cool1);
    // Instead, call reset, which will reset all of the transient memory
    // Be careful though - if the memory allocated has pointers to other memory,
    // make sure to release them before calling Reset().
    ResetTransientMemory();
    
    // Or maybe just allocate based on type instead
    // could also specify number of elements (for array-type allocations)
    // and the memory type
    cool1 = jalloc<CoolStruct>();
    // free works the same as before.
    jfree(cool1);
    
    // Let's create a small Free List Allocator to demonstrate other types of
    // allocation approaches
    size_t free_size = _KB(1);
    void *free_start = jalloc(free_size);
    FreeListAllocator fla = FreeListAllocator(free_size, free_start);
    
    // first, allocate and free directly from the allocator:
    cool1 = (CoolStruct*)fla.Allocate(sizeof(CoolStruct), __alignof(CoolStruct));
    fla.Free(cool1);
    
    // However, jengine::mm::jalloc/jfree can be used instead
    // could also specify number of elements (for array-type allocations)
    // and the memory type
    cool1 = jalloc<CoolStruct>(fla);
    jfree(fla, cool1);
    
    // Don't forget to free all memory in permanant storage, or else an
    // assertion will be thrown at MemoryAllocator shutdown
    fla.~FreeListAllocator(); // not necessary, but checks to make sure all allocations have been freed
    jfree(free_start); // free the allocator from permanant storage
    
    ShutdownMemoryManager();
    
    return (0);
}