@echo off
setlocal EnableDelayedExpansion

:: Project directories
SET HOST_DIR=%~dp0
set HOST_DIR=%HOST_DIR:~0,-1%
SET INC_DIR=%HOST_DIR%\inc
SET SRC_DIR=%HOST_DIR%\src
SET BUILD_DIR=%HOST_DIR%\build

:: Lib, EXE, and source names
SET PLAT_SRC=%HOST_DIR%\example.cpp

SET PLAT_EXE=%BUILD_DIR%\mm_example

:: Linking and Compiling info
SET CFLAGS=/Zi /std:c++17 /EHsc

SET INC=/I%INC_DIR% /I%SRC_DIR%

::--------------------------------------------------
:: Build commands
SET NONE_BUILD=""
SET CLEAN_BUILD=clean

:: Module Build commands
SET PLAT_BUILD=plat

::--------------------------------------------------

GOTO :MAIN
EXIT /B %ERRORLEVEL%

:: Helper Functions
::--------------------------------------------------
:: Creates the build directory for all files
:BUILD
    echo Creating build directory...
    1>NUL md %BUILD_DIR%

    1>NUL md %BUILD_DIR%\src
       
	EXIT /B 0

:: Removes the build directory. Clears all build files
:Clean
    echo Cleaning build directory...
    1>NUL del /f /s /q %BUILD_DIR%
    EXIT /B 0

:BuildPlatform
    echo Building platform layer...
    pushd %BUILD_DIR%\%PLAT%
        cl /MTd %CFLAGS% %INC% %PLAT_SRC% /Fe%PLAT_EXE% /link %PLAT_LIB% 
    popd
    EXIT /B 0

:: Entry Point
::-----------------------------------------------
:MAIN
    IF "%1" == %NONE_BUILD% (
        echo Building everything...	

        IF NOT EXIST %BUILD_DIR% (
            CALL :Build
        )

        CALL :Clean
        CALL :BuildPlatform
        EXIT /B %ERRORLEVEL%
    )

    IF NOT EXIST %BUILD_DIR% (
        CALL :BUILD
    )

    IF %1 == %PLAT_BUILD% (
        CALL :BuildPlatform
        EXIT /B %ERRORLEVEL%
    )

    IF %1 == %CLEAN_BUILD% (
        CALL :Clean
        EXIT /B %ERRORLEVEL%
    )

    